type point = {x:float; y:float};;
type triangle = {p1:point; p2:point; p3:point};;
type point_set = point list;;
type triangle_set = triangle list;;

(*get_x p gives the projection of p on x
  p : point
  get_x p : float*)
let get_x p = 
  p.x
;;

(*get_y p gives the projection of p on y
  p : point
  get_y p : float*)
let get_y p = 
  p.y
;;

(*get_p1 t gives the projection of t on p1
  t : triangle
  get_p1 t : point*)
let get_p1 t = 
  t.p1
;;

(*get_p2 t gives the projection of t on p2
  t : triangle
  get_p2 t : point*)
let get_p2 t = 
  t.p2
;;

(*get_p3 t gives the projection of t on p3
  t : triangle
  get_p3 t : point*)
let get_p3 t = 
  t.p3
;;

(*create_point x y creates a point with an x projection x and an y projection y
  x : float
  y : float
  create_point x y : point*)
let create_point x y = 
  {x = x; y = y}
;;

(*create_triangle p1 p2 p3 creates a triangle defined with the points p1 p2 p3
  p1 : point
  p2 : point 
  p3 : point
  create_triangle p1 p2 p3 : triangle*)
let create_triangle p1 p2 p3 = 
  {p1 = p1; p2 = p2; p3 = p3}
;;

(*print_point point prints the point point
  point : point
  print_point point : unit*)
let print_point point =
  print_string "(x:";
  print_float point.x;
  print_string ",y:";
  print_float point.y;
  print_string ")"
;;

(*is_empty_point_set points returns true if points is empty, false if not
  points : point_set
  is_empty_point_set points : bool*) 
let is_empty_point_set points = 
  match points with
    |[] -> true
    |_ -> false
;;

(*is_empty_triangle_set triangles returns true if triangles is empty, false if not
  triangles : triangle_set
  is_empty_triangle_set triangles : bool*)
let is_empty_triangle_set triangles = 
  match triangles with
    |[] -> true
    |_ -> false
;;

(*empty_triangle_set () creates an empty triangle set
  no argument
  empty_triangle_set () : unit*)
let empty_triangle_set () = 
  []
;;

(*car_triangle_set triangles returns the first triangle in triangles. It fails if triangles is empty
  triangles : triangle_set
  car_triangle_set triangles : triangle*)
let car_triangle_set triangles = 
  match triangles with 
    |[] -> failwith "car_triangle_set"
    |triangle::_ -> triangle
;;

(*cdr_triangle_set triangles returns the tail of the list triangles. It fails if triangles is empty
  triangles : triangle_set
  cdr_triangle_set triangles : triangles*)
let cdr_triangle_set triangles = 
  match triangles with 
    |[] -> failwith "cdr_triangle_set"
    |_::next -> next
;;

(*cons_triangle_set triangle triangles adds triangle in the first position of triangles
  triangle : triangle
  triangles : triangles
  cons_triangle_set triangle triangles : triangles*)
let cons_triangle_set triangle triangles =
  triangle::triangles
;;

(*empty_point_set () returns an empty point_set
  no argument
  empty_point_set () : point_set*)
let empty_point_set () = 
  []
;;

(*cdr_point_set points returns the tail of the list points. It fails if points is empty
  points : point_set
  cdr_point_set points : points*)
let cdr_point_set points = 
  match points with 
    |[] -> failwith "cdr_point_set"
    |_::next -> next
;;

(*car_point_set points returns the first point in points. It fails if points is empty
  points : point_set
  car_point_set points : point*)
let car_point_set points = 
  match points with 
    |[] -> failwith "car_point_set"
    |point::_ -> point
;;

(*cons_point_set point points adds point in the first position of points
  point : point
  points : points
  cons_point_set point points : points*)
let cons_point_set point points =
  point :: points
;;

(*print_triangle triangle prints the triangle triangle
  triangle : triangle
  print_triangle triangle : unit*)
let print_triangle triangle =
  print_string "(p1:";
  print_point triangle.p1 ;
  print_string ",p2:";
  print_point triangle.p2;
  print_string ",p3:";
  print_point triangle.p3;
  print_string ")";
  print_newline ()
;;

(* print_point_list points prints the point_set points
  points : point_set
  print_point_list points : unit*)
let rec print_point_list point_list =
  match point_list with
    |[] -> print_string("fin"); 
           print_newline()
    |p::t -> print_string("x : "); 
             print_float(get_x p); 
             print_string(" y : "); 
             print_float(get_y p);
             print_newline();
             print_point_list t
;;

(*equal_point point1 point2 tests if the points point1 and point2 are the same
  point1 : point
  point2 : point
  equal_point point1 point2 : bool*)
let equal_point point1 point2 =
  (int_of_float(get_x point1) == int_of_float(get_x point2) &&
  int_of_float(get_y point1) == int_of_float(get_y point2))
;;

(*is_point_in_set point points tests if the point point is in the set of point points
  point : point
  points : point_set 
  is_point_in_set point points : bool*)
let rec is_point_in_list point point_list =
  match point_list with
    |[] -> false
    |point2::tail -> 	if equal_point point point2 then true
                      else is_point_in_list point tail
;;

(*random nb max_x max_y creates a random set of point with a x projection between 0 and max_x-1 and an y projection between 0 and max_y-1. There is nb points in set
  nb : int
  max_x : int
  max_y : int
  random nb max_x max_y : point_set*)  
let random nb max_x max_y =
  Random.self_init() ;
  let point_list = ref [] and i = ref 0 in
  while !i < nb do
    let point = create_point(Random.float (float_of_int(max_x))) 
                            (Random.float (float_of_int(max_y)))
    in
    if not(is_point_in_list point !point_list) 
      then 
        begin
          incr i;
           point_list := point::!point_list
      end
    done;
  (!point_list : point_set)
;;

(*is_direct_direction point1 point2 point3 tests if triangle created from point1, point2 and point3 are in the direct direction
  point1 : point
  point2 : point
  point3 : point
  is_direct_direction point1 point2 point3 : bool*)
let is_direct_direction point1 point2 point3 =
((get_x(point2) -. get_x(point1))*.(get_y(point3) -. get_y(point1)) -. 
	(get_x(point3) -. get_x(point1))*.(get_y(point2) -. get_y(point1))) > (0.)
;;

(*border triangles returns the list of pair (p,q) such as p and q are two consecutive points on the border of the convex area delimited by the set of triangle triangles
  triangles : triangle_set
  border triangles : (point * point) list*)
let border triangles =
  let equal_edges edge1 edge2 =
    let (point1,point2)=edge1
    and (point3,point4)=edge2
    in
    (equal_point point1 point3 && (equal_point point2 point4)) ||
    (equal_point point1 point4 && (equal_point point2 point3))
  in

  let rec add_edge edge edges = 
    match edges with
      |[] -> [(edge,1)]
      |(edge2,nb_seen)::next when equal_edges edge edge2 -> (edge2,nb_seen+1)::next
      |couple::next -> couple:: (add_edge edge next)
  in

  let edges = ref [] in

  let rec add_triangle triangles =
    match triangles with
      |[] -> ()
      |triangle::next ->
                       (edges := (add_edge (get_p1(triangle), get_p2(triangle)) !edges);
                        edges := (add_edge (get_p2(triangle), get_p3(triangle)) !edges);
                        edges := (add_edge (get_p3(triangle), get_p1(triangle)) !edges);
                        add_triangle next)
  in

  let rec border_aux edges =
    match edges with
      |[] -> []
      |(edge,1)::next -> (edge:: (border_aux next))
      |_::next -> border_aux next
  in
  add_triangle triangles;
  border_aux !edges
;;

(*append_triangle_set first second creates a set of triangle by concatenate first and second
  first : triangle_set
  second : triangle_set
  append_triangle_set first second : triangle_set*)
let append_triangle_set first second =
  first @ second
;;

(*surface_triangle triangle computes the surface of the triangle triangle
  triangle : triangle
  surface_triangle triangle : float*)
let surface_triangle triangle =
  let point1 = get_p1 triangle
  and point2 = get_p2 triangle
  and point3 = get_p3 triangle
  in
  let surface = 
    ((get_x point2) -. (get_x point1)) *. ((get_y point3) -. (get_y point1)) -.
    (((get_y point2) -. (get_y point1)) *. ((get_x point3) -. (get_x point1)))
  in
  0.5 *. (max surface (-. surface))
;;

(*cumulated_surface triangle_set computes the total surface of the set of triangle triangle_set
  triangle_set : triangle_set
  cumulated_surface triangle_set : float*)
let cumulated_surface triangle_set =
  let compteur = ref 0. in
  let list = ref triangle_set in
  while !list <> (empty_triangle_set()) do
    compteur := !compteur +. (surface_triangle (car_triangle_set !list));
    list := (cdr_triangle_set !list)
  done;
  !compteur
;;
