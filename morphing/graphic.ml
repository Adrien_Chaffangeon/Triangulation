open Geometry;;
open Matrix;;
open Graphics;;
let max_x = 600;;
let max_y = 600;;

(*open_window max_x max_y opens a window with width equal to max_x and height equal to max_y
  max_x : int
  max_y : int
  open_window max_x max_y : unit*)
let open_window max_x max_y = 
  open_graph (" " ^ string_of_int max_x ^ "x" ^ string_of_int max_y ^ "--0+0")
;;

(*draw_cross point draws a cross in place of the point point
  point : point
  draw_cross point : unit*)
let draw_cross point =
  let x = int_of_float (get_x point)
  and y = int_of_float (get_y point)
  and width = 5
  in
  moveto (x-width) y;
  lineto (x+width) y;
  moveto x (y-width);
  lineto x (y+width)
;;

(*draw_points points draws the set of points points
  points : point_set
  draw_points points : unit*)
let draw_points points =
  let rec aux points =
    if is_empty_point_set points
    then ()
    else   (draw_cross (car_point_set points);
         aux (cdr_point_set points));
  in
  aux points
;; 

(*draw_triangle p1 p2 p3 color draws the triangle represented by the three points p1 p2 p3 in the color color with black borders 
  p1 : point
  p2 : point
  p3 : point
  color : color
  draw_triangle p1 p2 p3 color : unit*)
let draw_triangle p1 p2 p3 color =
  set_color color;
  let p1'=(int_of_float (get_x p1), int_of_float (get_y p1))
  and p2'=(int_of_float (get_x p2), int_of_float (get_y p2))
  and p3'=(int_of_float (get_x p3), int_of_float (get_y p3))
  in  
  fill_poly [|p1';p2';p3'|];
  set_color black;
  moveto (fst p1') (snd p1');
  lineto (fst p2') (snd p2');
  lineto (fst p3') (snd p3');
  lineto (fst p1') (snd p1')
;;

(*draw_complete_triangles p1 p2 p 3 draws the triangle represented by the three points p1 p2 p3 in the color color without borders 
  p1 : point
  p2 : point
  p3 : point
  color : color
  draw_complete_triangle p1 p2 p3 color : unit*)
let draw_complete_triangle p1 p2 p3 color =
  set_color color;
  let p1'=(int_of_float (get_x p1), int_of_float (get_y p1))
  and p2'=(int_of_float (get_x p2), int_of_float (get_y p2))
  and p3'=(int_of_float (get_x p3), int_of_float (get_y p3))
  in  
  fill_poly [|p1';p2';p3'|];
  moveto (fst p1') (snd p1');
  lineto (fst p2') (snd p2');
  lineto (fst p3') (snd p3');
  lineto (fst p1') (snd p1')
;;

(*draw_triangles triangles draws each triangle of the set triangles
  triangles : triangle_set
  draw_triangles triangles : unit*)
let draw_triangles triangles =
  clear_graph ();
  let color_vect =[|blue;red;green;yellow;magenta;cyan|] in 
  let rec aux triangles =
    if is_empty_triangle_set triangles
    then ()
    else
      (
  let triangle1 = car_triangle_set triangles 
  and triangles2 = cdr_triangle_set triangles in
  draw_triangle (get_p1 triangle1) (get_p2 triangle1) (get_p3 triangle1) color_vect.(Random.int 6) ;
  aux triangles2
      );
  in
  aux triangles
;;

(*draw_line point1 point2 draws a line between point1 point2
  point1 : point
  point2 : point
  draw_line point1 point2 : unit*)
let draw_line point1 point2 =
  let x1 = int_of_float (get_x point1)
  and x2 = int_of_float ( get_x point2)
  and y1 =  int_of_float (get_y point1)
  and y2 =  int_of_float (get_y point2)
  in
  set_color red;
  moveto x1 y1 ;
  lineto x2 y2
;;
       
(*draw_edges edges draws each edge in the set edges
  edges : (point * point) list
  draw_edges edges : unit*)
let draw_edges edges =
  let rec draw_edge edges =
    match edges with
      |[]-> ()
      |edge::m ->  (let (point1,point2)=edge in
             draw_line point1 point2;
             draw_edge m)
  in
  print_string "edges";
  draw_edge edges
;;
