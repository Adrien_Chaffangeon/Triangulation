open Graphics;;
open Graphic;;
open Matrix;;
open Geometry;;

(*add_point triangles point adds the point point to the set triangles
  triangles : triangle_set
  point : point
  add_point triangles point : triangle_set*)
let add_point triangles point = 
  let triangles2 = ref (empty_triangle_set()) in
  let rec remove_triangle triangles = 
    if is_empty_triangle_set triangles
    then (empty_triangle_set())
    else  (let triangle = (car_triangle_set triangles) in
         if is_direct_direction (get_p1 triangle) (get_p2 triangle) (get_p3 triangle)
         then  (if in_circle triangle point
             then  (triangles2 := cons_triangle_set (car_triangle_set triangles) !triangles2; 
                 remove_triangle (cdr_triangle_set triangles)
                )
             else   (cons_triangle_set triangle (remove_triangle (cdr_triangle_set triangles)) ) 
            ) 
         else  (let triangle' = create_triangle  (get_p2 triangle) (get_p1 triangle) (get_p3 triangle) in
             if in_circle triangle' point
             then   (triangles2 := cons_triangle_set triangle' !triangles2; 
                 remove_triangle (cdr_triangle_set triangles))
             else (cons_triangle_set triangle' (remove_triangle (cdr_triangle_set triangles))) 
            )
        ) 
  in
  let triangles3 = remove_triangle triangles in 
  let points = border !triangles2 in
  let triangles4 = ref (empty_triangle_set()) in
  let rec aux list_points_couple = match list_points_couple with
    |[] -> ()
    |(a,b)::suite ->   triangles4 := cons_triangle_set (create_triangle a b point) !triangles4; 
              aux suite
  in
  aux points;
  let triangles_res = append_triangle_set triangles3 !triangles4 in
  triangles_res
;;

(*delaunay points max_x max_y builds the Delaunay's triangulation of the point cloud points  initializing with a mesh of work space between (0,0) and (max_x, max_y)
  points : point_set
  max_x : int
  max_y : int
  delaunay points max_x max_y : triangle_set*)
let delaunay points max_x max_y = 
  let triangles = 
    ref (cons_triangle_set  (create_triangle (create_point 0. 0.) 
                (create_point (float_of_int(max_x-1)) 0.) 
                (create_point (float_of_int(max_x-1)) (float_of_int(max_y-1)))
                )
                (cons_triangle_set  (create_triangle (create_point 0. 0.) 
                          (create_point 0. (float_of_int(max_y-1)))
                          (create_point (float_of_int(max_x-1)) (float_of_int(max_y-1)))
                          )
                          (empty_triangle_set())
                )
      )
  in
  let rec aux points = 
    if is_empty_point_set points
    then ()
    else (triangles := add_point (!triangles) (car_point_set points);
        aux (cdr_point_set points));
  in
  aux points;
  !triangles
;;
