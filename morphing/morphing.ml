open Graphics;;
open Graphic;;
open Matrix;;
open Geometry;;
open Projet;;
open Sys
let choice = int_of_string(argv.(1));;
let nb_step = int_of_string(argv.(2));;

(* color_to_rgb (c: Graphics.color) allows to skip from a color to its rgb code
   c : Graphics.color (which is, in fact, int)
   color_to_rgb (c: Graphics.color) : (int * int * int)
*)
let color_to_rgb (c: Graphics.color) =
  let r = c /65536
  and g = c / 256 mod 256
  and b = c mod 256  
  in
  (r,g,b)
;;

(* load_image_from_txt file_name allows to get an image from a file called file_name
   fiel_name : string
   load_image_from_txt file_name : Graphic_image
*)
let load_image_from_txt file_name =
  let f = open_in file_name in
  let n = int_of_string (input_line f) 
  and p = int_of_string (input_line f)
  in
  open_window p n ;
  resize_window p n ;
  let image = create_image p n in
  let matrix = dump_image image in
  for i=0 to n-1 do
    for j=0 to p-1 do
      let r = int_of_string (input_line f) 
      and g = int_of_string (input_line f) 
      and b = int_of_string (input_line f)
      in
      matrix.(i).(j)<- rgb r g b 
    done;
  done;
  let image = make_image matrix  in
  
  draw_image image 0 0;
  image
;;

(* the 14 following functions create several images *)

let get_Milner () = load_image_from_txt "Milner_image.txt";;

let get_Knuth () = load_image_from_txt "Knuth_image.txt";;

let get_raspberry () = load_image_from_txt "raspberry_image.txt";; 

let get_strawberry () = load_image_from_txt "strawberry_image.txt";;

let get_milner () = load_image_from_txt "milner_image.txt";; 

let get_knuth () = load_image_from_txt "knuth_image.txt";;

let get_star () = load_image_from_txt "star_image.txt";; 

let get_square () = load_image_from_txt "square_image.txt";;

let creation_points_cloud1 () =
  [|(0,0);(0,500);(500,0);(500,500);(200,200);(200,400)|]
;;

let creation_points_cloud2 () =
  [|(0,0);(0,500);(500,0);(500,500);(400,400);(400,300)|]
;;

let create_points_strawberry () = 
  let stra =
    [|(0,0);(99,0);(0,99);(99,99);
      (1,88);(0,84);(1,77);(2,74);(3,69);
      (5,64);(8,58);(12,50);(13,45);(15,40);
      (18,34);(23,28);(28,23);(32,21);(40,19);
      (44,19);(49,20);(54,21);(60,22);(58,15);
      (61,9);(68,4);(65,9);(65,14);(67,21);
      (70,15);(79,11);(77,19);(74,25);(77,23);
      (82,23);(85,16);(85,28);(82,33);(91,32);
      (97,32);(92,43);(83,43);(98,43);(84,45);
      (85,49);(86,55);(85,62);(82,70);(78,76);
      (73,81);(66,84);(61,86);(56,87);(49,88);
      (43,89);(37,92);(30,93);(23,95);(16,95);
      (9,94);(4,91);(65,25);(71,30);(77,36);
      (72,34);(28,31);(35,36);(45,37);(55,30);
      (48,25);(18,45);(30,48);(41,47);(54,45);
      (60,37);(66,43);(70,52);(76,48);(63,62);
      (44,59);(34,58);(17,65);(10,69);(11,84);
      (24,84);(40,83);(40,71);(26,72);(60,70);
      (78,58);(54,77);(72,69);(24,39);(12,91);
      (39,22);(77,42);(10,78);(46,76);(20,36);
      (77,29);(18,53);(54,53);(50,66);(32,78);
    |]
  in
  for k=0 to 99 do
    stra.(k) <- (fst(stra.(k)),99-snd(stra.(k)))
  done;
  stra
;;

let create_points_raspberry () = 
  let rasp = 
    [|(0,0);(99,0);(0,99);(99,99);
      (12,82);(11,77);(8,71);(7,66);(6,60);
      (8,56);(8,47);(9,41);(12,35);(15,31);
      (18,27);(22,23);(27,19);(29,18);(39,17);
      (44,16);(51,17);(53,18);(55,20);(56,15);
      (58,9);(67,4);(61,9);(59,15);(60,21);
      (65,17);(73,15);(66,20);(64,26);(71,24);
      (79,23);(88,18);(78,31);(73,39);(78,42);
      (89,38);(82,45);(87,46);(94,35);(84,50);
      (83,54);(85,61);(83,68);(80,75);(76,80);
      (71,83);(68,88);(64,89);(58,89);(54,92);
      (50,93);(41,95);(33,91);(25,91);(21,88);
      (18,87);(16,85);(60,24);(64,28);(72,38);
      (66,36);(26,29);(31,32);(38,34);(50,27);
      (47,20);(16,46);(30,46);(39,44);(53,45);
      (57,35);(64,45);(68,53);(74,53);(63,64);
      (50,58);(39,55);(24,60);(14,58);(22,76);
      (32,79);(48,83);(46,72);(35,65);(64,70);
      (77,60);(64,80);(75,71);(22,43);(25,86);
      (36,20);(77,48);(21,68);(56,75);(18,34);
      (70,31);(21,52);(54,51);(51,65);(36,72);
    |]
  in
  for k=0 to 99 do
    rasp.(k) <- (fst(rasp.(k)),99-snd(rasp.(k)))
  done;
  rasp
;;

let image1 () =
  open_window 501 501;
  let triangles = ref (empty_triangle_set ()) in
  triangles := cons_triangle_set (create_triangle (create_point 0. 0.) (create_point 0. 500.) (create_point 200. 400.)) !triangles;
  triangles := cons_triangle_set (create_triangle (create_point 0. 0.) (create_point 500. 0.) (create_point 200. 200.)) !triangles;
  triangles := cons_triangle_set (create_triangle (create_point 0. 0.) (create_point 200. 200.) (create_point 200. 400.)) !triangles;
  triangles := cons_triangle_set (create_triangle (create_point 0. 500.) (create_point 500. 500.) (create_point 200. 400.)) !triangles;
  triangles := cons_triangle_set (create_triangle (create_point 500. 0.) (create_point 500. 500.) (create_point 200. 200.)) !triangles;
  triangles := cons_triangle_set (create_triangle (create_point 500. 500.) (create_point 200. 400.) (create_point 200. 200.)) !triangles;
  draw_triangles !triangles;
  get_image 0 0 501 501
;;

let image2 () =
  open_window 501 501;
  let triangles = ref (empty_triangle_set ()) in
  triangles := cons_triangle_set (create_triangle (create_point 0. 0.) (create_point 0. 500.) (create_point 400. 300.)) !triangles;
  triangles := cons_triangle_set (create_triangle (create_point 0. 0.) (create_point 500. 0.) (create_point 400. 400.)) !triangles;
  triangles := cons_triangle_set (create_triangle (create_point 0. 0.) (create_point 400. 400.) (create_point 400. 300.)) !triangles;
  triangles := cons_triangle_set (create_triangle (create_point 0. 500.) (create_point 500. 500.) (create_point 400. 300.)) !triangles;
  triangles := cons_triangle_set (create_triangle (create_point 500. 0.) (create_point 500. 500.) (create_point 400. 400.)) !triangles;
  triangles := cons_triangle_set (create_triangle (create_point 500. 500.) (create_point 400. 300.) (create_point 400. 400.)) !triangles;
  draw_triangles !triangles;
  get_image 0 0 501 501
;;

(* create_matrix_barycentre vect_pts1 vect_pts2 n creates a matrix which contains the position of all the points at each step (there is n of them) of the morphing, from pts1 to pts2
   vect_pts1 : (int * int) array
   vect_pts2 : (int * int) array
   n : int
   create_matrix_barycentre vect_pts1 vect_pts2 n : point array array
*)
let create_matrix_barycentre vect_pts1 vect_pts2 n =
  let nb_points = Array.length vect_pts1 in
  if nb_points <> (Array.length vect_pts2) then failwith "create_matrix_barycentre"
  else
    let matrix = Array.make (n+1) (Array.make nb_points (create_point 0. 0.)) in

    for i = 1 to n do
      matrix.(i) <- Array.make nb_points  (create_point 0. 0.)
    done;
    for i=0 to n do
      for j=0 to (nb_points-1) do
	let x1= float_of_int (fst vect_pts1.(j))
	and x2= float_of_int (fst vect_pts2.(j))
	and y1= float_of_int (snd vect_pts1.(j))
	and y2= float_of_int (snd vect_pts2.(j))
	and alpha = (float_of_int i) /. (float_of_int n)
	in
	let x= ((1.-.alpha) *. x1) +. (alpha *. x2)
	and y= ((1.-.alpha) *. y1) +. (alpha *. y2)
	in
	matrix.(i).(j) <- create_point x y ;
      done;
    done;
    matrix
;;

(* create_point_set_from_matrix matrix creates a point_set (from the moddle step) on which we will build a delaunay tilling
   matrix : point array array
   create_point_set_from_matrix matrix : point_set
*)
let create_point_set_from_matrix matrix =
  let points = ref (empty_point_set ()) 
  and average = (Array.length matrix)/2
  in
  for j=0 to (Array.length matrix.(0)-1) do
    points := cons_point_set matrix.(average).(j) !points 
  done;
  !points
;;

exception Absent_point of point;; 

(* convertion_triangle_set_absolute_triangle matrix triangle converts the points of the triangle_set into points of the matrix (at the average step)
   matrix : point array array
   triangles : triangle_set
   convertion_triangle_set_absolute_triangle matrix triangles : (int * int * int) list
*)     

let convertion_triangle_set_absolute_triangle matrix triangles =
  let triangles_absolute = ref [] in
  let rec add_triangle triangles =
    if is_empty_triangle_set triangles
    then ()
    else
      (let get_ind matrix point = 
	 let i = ref (-1)
	 and m = (Array.length matrix) / 2
	 and n = (Array.length matrix.(0))
	 and k = ref 0 
	 in
	 while (!k<n && !i=(-1)) do
	   if equal_point matrix.(m).(!k) point
	   then (i :=!k; k := n)
	   else k := !k + 1
	 done;
	 if (!i=(-1))
	 then raise (Absent_point point)
	 else !i
       in
       let rec transform triangle_set = 
	 if is_empty_triangle_set triangle_set
	 then ()
	 else
	   (
	     let t = car_triangle_set triangle_set in
	     triangles_absolute := (get_ind matrix (get_p1 t),
				    get_ind matrix (get_p2 t),
				    get_ind matrix (get_p3 t)) 
	     :: !triangles_absolute ;
	     transform (cdr_triangle_set triangle_set)
	   )
       in
       transform triangles	 
      )
  in
  add_triangle triangles;
  !triangles_absolute
;;

(* belong_to_the_triangle triangle pixel tells if the pixel belongs to the triangle
   triangle : triangle
   pixel : (int * int)
   let belong_to_the_triangle triangle pixel : bool
*)
let belong_to_the_triangle triangle pixel =
  let ab = (int_of_float ((get_x (get_p2 triangle))-.(get_x (get_p1 triangle))),int_of_float ((get_y (get_p2 triangle))-.(get_y (get_p1 triangle))))
  and bc = (int_of_float ((get_x (get_p3 triangle))-.(get_x (get_p2 triangle))),int_of_float ((get_y (get_p3 triangle))-.(get_y (get_p2 triangle))))
  and ac = (int_of_float ((get_x (get_p3 triangle))-.(get_x (get_p1 triangle))),int_of_float ((get_y (get_p3 triangle))-.(get_y (get_p1 triangle))))
  and am = ((int_of_float (get_x (get_p1 triangle)))-(fst pixel),(int_of_float (get_y (get_p1 triangle)))-(snd pixel))
  and bm = ((int_of_float (get_x (get_p2 triangle)))-(fst pixel),(int_of_float (get_y (get_p2 triangle)))-(snd pixel))
  and cm = ((int_of_float (get_x (get_p3 triangle)))-(fst pixel),(int_of_float (get_y (get_p3 triangle)))-(snd pixel))
  in
(   (((fst ab) * (snd am) - (fst am) * (snd ab)) * ((fst am) * (snd ac) - (fst ac) * (snd am))) >=0 &&
    (-((fst ab) * (snd bm) - (fst bm) * (snd ab)) * ((fst bm) * (snd bc) - (fst bc) * (snd bm))) >=0 &&
    (((fst ac) * (snd cm) - (fst cm) * (snd ac)) * ((fst cm) * (snd bc) - (fst bc) * (snd cm))) >=0 )

;; 

(* pixels_in_triangle point1 point2 point3 max_x max_y lists all the pixel in the triangle created from the three points point1, point2, point3
   point1 : point
   point2 : point
   point3 : point
   max_x : int
   max_y : int
   pixels_in_triangle point1 point2 point3 max_x max_y : (int * int) list
*)
let pixels_in_triangle point1 point2 point3 max_x max_y =  
  let minimum_x = int_of_float (min (min (get_x point1) (get_x point2)) (get_x point3))
  and maximum_x = int_of_float (max (max (get_x point1) (get_x point2)) (get_x point3))
  and minimum_y = int_of_float (min (min (get_y point1) (get_y point2)) (get_y point3))
  and maximum_y = int_of_float (max (max (get_y point1) (get_y point2)) (get_y point3))
  in
  if (minimum_x < 0 || minimum_y < 0 || maximum_x >= max_x || maximum_y >= max_y)
  then failwith "point out of the window"
  else 
    (
      let l = ref [] in
      for i=minimum_x to maximum_x do
	for j=minimum_y to maximum_y do
	  if belong_to_the_triangle (create_triangle point1 point2 point3) (i,j)
	  then l := (i,j) :: !l
	done
      done;
      !l
    )
;;

(* morphing image1 image2 points_cloud1 points_cloud2 n creates a morphing between image1 and image2 from the pointsset points_cloud1 and points_cloud2 in n steps
   image1 : Graphics.image
   image2 : Graphics.image
   points_cloud1 : (int * int) array
   points_cloud2 : (int * int) array
   n : int
*)
let morphing image1 image2 points_cloud1 points_cloud2 n=
  let matrix_image1 = dump_image image1 and matrix_image2 = dump_image image2 in
  let max_x = Array.length matrix_image1.(0) and max_y = Array.length matrix_image1 in 
  open_window max_x max_y;
  resize_window max_x max_y;
  let matrix = create_matrix_barycentre (points_cloud1) (points_cloud2) n in
  let points = create_point_set_from_matrix matrix in
  let triangles = delaunay points  max_x max_y in
  let triangle_absolute = convertion_triangle_set_absolute_triangle matrix triangles in
  let morphing_vect = Array.make (n+1) (image1) in

  (* average_color pixel_list k amkes an average of each color (red, green and blue) of the pixel in the pixel_list at the k_th step
     pixel_list : (int * int) list
     k : int
  *)
  let average_color pixel_list k =
    let r= ref 0. and g = ref 0. and b = ref 0. and number_of_pixels = ref 0. in

    let rec aux pixel_list =
      match pixel_list with
      |[] -> ()
      |pixel::next ->
	 ( 
	   let (r1,g1,b1) = color_to_rgb matrix_image1.(snd pixel).(fst pixel)
	   and (r2,g2,b2) = color_to_rgb matrix_image2.(snd pixel).(fst pixel)
	   and alpha = float_of_int k /. (float_of_int n)
	   in
	   let r'=  (1. -. alpha )*.(float_of_int r1) +. alpha *. ( float_of_int r2)
	   and g'=  (1. -. alpha )*.(float_of_int g1) +. alpha *. ( float_of_int g2)
	   and b'=  (1. -. alpha )*.(float_of_int b1) +. alpha *. ( float_of_int b2)
	   in
	   r:= !r +. r';
	   g:= !g +. g';
	   b:= !b +. b';
	   number_of_pixels := 1. +. !number_of_pixels;
	   
	   aux next;
	 )
    in

    aux pixel_list;
    rgb (int_of_float (!r/. !number_of_pixels )) (int_of_float (!g/. !number_of_pixels)) (int_of_float (!b/. !number_of_pixels))
  in


  (* draw_triangle_absolute triangle k image draws the triangle (its points are in the matrix) at the k_th step on the current image
     triangle = (int * int * int) 
     k : int
     image : Graphic.image
     draw_triangle_absolute triangle k image : image
  *)
  let draw_triangle_absolute triangle k image =
    clear_graph ();
    let (p1,p2,p3)=triangle in    
    let pixel_list = pixels_in_triangle matrix.(k).(p1) matrix.(k).(p2) matrix.(k).(p3) max_x max_y  in
    let color = average_color pixel_list k  in 
    draw_image image 0 0;
    draw_complete_triangle matrix.(k).(p1) matrix.(k).(p2) matrix.(k).(p3) color;
    get_image 0 0 max_x max_y
  in

  (* draw_morphing_step k triangle_absolute draws every triangle in triangle absolute at the k_th step
     k : int
     triangle_absolute : (int * int * int) list
     draw_morphing_step k triangle_absolute : unit
  *)
  let rec draw_morphing_step k triangle_absolute =
  match triangle_absolute with
  |[] -> ()
  |triangle::next ->
     (
       draw_morphing_step k next;
       morphing_vect.(k) <- draw_triangle_absolute triangle k morphing_vect.(k)
     )
  in  

  for k=0 to n do
    draw_morphing_step k triangle_absolute
  done;
  morphing_vect
;;

let rotation image =
  let matrix = dump_image image in 
  let n= Array.length matrix
  and p= Array.length matrix.(0)
  in
  for i=0 to (n-1)/2 do
    for j=0 to p-1 do
      let temp = matrix.(i).(j) in
      matrix.(i).(j) <- matrix.(n-1-i).(j);
      matrix.(n-1-i).(j)<-temp
    done;
  done;
  make_image matrix
;;


(* display_gif gif displays gif gif
   gif : Graphic.image
   display_gif gif : unit
*)
let display_gif gif =
  let matrix = dump_image gif.(0) in
  let max_y = Array.length matrix and max_x = Array.length matrix.(0) in
  open_window max_x max_y ;
  resize_window max_x max_y;
  let n= Array.length gif in
  let key = ref (wait_next_event [Key_pressed])
  and k = ref 0 in
  
  while (!key).key <> 'q'  do
    draw_image (rotation(gif.(!k))) 0 0;
    key := wait_next_event [Key_pressed];
    match (!key).key with
    |'e' -> if !k <> n-1 then incr k
    |'a' -> if !k <> 0 then decr k
    |_ -> ()
  done
;;

(* belong_to_the_list l x tells wether or not x belongs to the list l
   l : 'a list
   x : 'a
   belong_to_the_list l x : bool
*)
let rec belong_to_the_list l x =
  match l with
  |[] -> false
  |y::_ when y=x -> true
  |_::next -> belong_to_the_list next x
;;

(* suppress_head l suppresses the head of the list l and sends it
   l : 'a list
   suppress_head l : 'a
*)
let suppress_head l =
  match !l with
  |[] -> failwith "no head"
  |x::next -> (l:=next;x)
;;


(* create_cloud_points_from_image image allows to create a vect of pixel from an image with the mouse (we select the points)
   image : Graphic.image
   create_cloud_points_from_image image : (int * int) vect
*)
let create_cloud_points_from_image image =
  let matrix = dump_image image in
  let max_y = Array.length matrix and max_x = Array.length matrix.(0) in
  open_window max_x max_y ;
  draw_image image 0 0;
  let pixel_list = ref []
  and number_of_pixels = ref 0 
  and key = ref (wait_next_event [Key_pressed])
  in
  while (!key).key <> 'q' do
    key := wait_next_event [Key_pressed;Button_down;Button_up];
    if (!key).button
    then
      (
	let x= (!key).mouse_x
	and y = (!key).mouse_y
	in
	if (not (belong_to_the_list !pixel_list (x,y)))
	then
	  (
	    print_string "("; print_int x;print_string ","; print_int y; print_string ")"; print_newline ();
	    pixel_list :=  (x,y):: !pixel_list ; set_color cyan ; plot x y; incr number_of_pixels
	  )
	else
	  (
	    print_string "("; print_int x;print_string ","; print_int y; print_string ")";
	    print_string "already present";print_newline ();
	  )
      )
    else
      (
	match (!key).key with
	|'x' -> let (x,y)= (suppress_head pixel_list) in
		set_color red;
		plot x y;
		decr number_of_pixels
		  
	|_ -> ()
      )
  done;
  let pixel_array = Array.make (!number_of_pixels) (0,0) in
  let rec fill_the_array pixel_list i=
    match pixel_list with
    |[] -> ()
    |pixel::next -> (pixel_array.(i)<-pixel;fill_the_array next (i+1))
  in
  fill_the_array !pixel_list 0;
  pixel_array
;;

(* cat file_name prints the file called file_name
   file_name : string
   cat file_name : unit
*)
let cat file_name =
  let f = open_in file_name in
  let rec cat_rec () =
    try
      print_string (input_line f); print_newline (); cat_rec ();
    with End_of_file -> close_in f
  in
  cat_rec ()
;;

(* write file_name text writes the text the file called file_name
   file_name : string
   text : unit
   write file_name text : unit
*)
let write file_name text =
  let f = open_out file_name in
  output_string f text;
  close_out f
;;

(* save_pixel_array array file_name writes a array of pixels into the file called file_name
   array : (int * int) array
   file_name : string
   save_pixel_array array file_name : unit
*)
let save_pixel_array array file_name =
  let f = open_out file_name 
  and n = Array.length array
  in
  output_string f (string_of_int n);
  output_string f "\n";
  for i=0 to n-1 do
    output_string f (string_of_int (fst array.(i)));
    output_string f "\n";
    output_string f (string_of_int (snd array.(i)));
    output_string f "\n";
  done;
  close_out f
;;

(* return_pixel_array_from_txt file_name gets an array of pixel from the file called file_name
   file_name : string
   return_pixel_array_from_txt file_name : (int * int) array
*)
let return_pixel_array_from_txt file_name =
  let f = open_in file_name in
  let n = int_of_string (input_line f) in
  let pixel_array = Array.make n (0,0) in
  for i=0 to n-1 do
    let x = int_of_string (input_line f) in
    let y = int_of_string (input_line f) in
    pixel_array.(i) <- (x,y)
  done;
  pixel_array
;;

(* hand_made_Milner_Knuth_cloud_points () saves the modification done on the picture in the file "Milner_Knuth.txt"
   No argument
   hand_made_Milner_Knuth_cloud_points () : unit()
*)
let hand_made_Milner_Knuth_cloud_points () =
  open_window 500 311 ;
  let img = get_Milner () in
  draw_image img 0 0;
  let img = get_Knuth () in
  draw_image img 251 0;
  let mk = get_image 0 0 500 311 in
  let vect = create_cloud_points_from_image mk in
  save_pixel_array (vect) "Milner_Knuth.txt"
;;
(* rift_Milner_Knuth n separates the picture of Milner and Knuth with n points
   n : int
   rift_Milner_Knuth n : ((int * int) array, (int * int) array
*)
let rift_Milner_Knuth n =
  let f = open_in ("Milner_Knuth" ^ string_of_int n ^".txt") in
  let n = int_of_string (input_line f) in
  let vect_for_Milner = Array.make (n/2+4) (0,0) in
  let vect_for_Knuth = Array.make (n/2+4) (0,0) in
  vect_for_Milner.(1) <- (0,310);
  vect_for_Knuth.(1) <- (0,310);
  vect_for_Milner.(2) <- (248,310);
  vect_for_Knuth.(2) <- (248,310);
  vect_for_Milner.(3) <- (248,0);
  vect_for_Knuth.(3) <- (248,0);
  for i=0 to n-1 do
    if i mod 2 = 0
    then
      (
	let x = int_of_string (input_line f) in
	let y = int_of_string (input_line f) in
	if x>=251 && x<=600
	then vect_for_Knuth.(i/2+4) <- (x-251,y);
      )
    else
      (
	let x = int_of_string (input_line f) in
	let y = int_of_string (input_line f) in
	if x>=0 && x<=249 then vect_for_Milner.((i-1)/2+4) <- (x,y);
      )
  done;
  (vect_for_Milner,vect_for_Knuth)
;;

(* display_cloud image cloud displays the image with the points in cloud
   image : Graphic_image
   cloud : (int * int) array
   display_cloud image cloud : unit
*)
let display_cloud image cloud =
  let matrix = dump_image image in
  let max_y = Array.length matrix and max_x = Array.length matrix.(0) in
  open_window max_x max_y ;
  resize_window max_x max_y;
  draw_image image 0 0;
  set_color cyan;
  for i=0 to (Array.length cloud)-1 do
    
    let (x,y) = cloud.(i) in
    if x<0 then failwith "banana"
    else plot x y ;
  done;
;;

(* merge_cloud milner1 milner2 knuth1 knuth2 merges the array milner1 and milner2, and knuth1 and knuth2
   milner1 : (int * int) array
   milner2 : (int * int) array
   knuth1 : (int * int) array
   knuth2 : (int * int) array
   merge_cloud milner1 milner2 knuth1 knuth2 : ((int * int) array, (int * int) array)
*)
let merge_cloud milner1 milner2 knuth1 knuth2 =
  let n1 = Array.length milner1
  and n2 = Array.length milner2
  in
  let  milner_merged = ref []
  and  knuth_merged = ref []
  and number = ref 0 
  in
  for i=0 to n1-1 do
    if not (belong_to_the_list !milner_merged milner1.(i)) && not (belong_to_the_list !knuth_merged knuth1.(i))
    then (milner_merged := milner1.(i) :: !milner_merged; knuth_merged := knuth1.(i) :: !knuth_merged; incr number)
  done;
  for i=0 to n2-1 do
    if not (belong_to_the_list !milner_merged milner2.(i)) && not (belong_to_the_list !knuth_merged knuth2.(i))
    then (milner_merged := milner2.(i) :: !milner_merged; knuth_merged := knuth2.(i) :: !knuth_merged;incr number)
  done;
  let milner = Array.make !number (0,0)
  and knuth = Array.make !number (0,0)
  in
  let rec fill l1 l2 i=
    match (l1,l2) with
    |([],[]) -> ()
    |(a::next1,b::next2) -> (milner.(i)<-a;knuth.(i) <- b;fill next1 next2 (i+1))
    |_ -> failwith "not the same size"
  in
  fill !milner_merged !knuth_merged 0 ;
  (milner,knuth)
;;

(* improve_Milner_Knuth_cloud_points ()  allows to create new points on the image
  No argument 
  improve_Milner_Knuth_cloud_points () : unit
*)
let improve_Milner_Knuth_cloud_points () =
  open_window 500 311 ;
  let vect1 = return_pixel_array_from_txt "Milner_Knuth3.txt" in
  let (m,k) = rift_Milner_Knuth 3 in
  let img = get_Milner () in
  draw_image img 0 0;
  let img = get_Knuth () in
  draw_image img 251 0;
  for i=0 to (Array.length m -1) do
    let (x1,y1) = m.(i)
    and (x2,y2) = k.(i)
    in
    set_color green ;
    plot x1 y1 ;
    plot (x2+251) (y2) 
  done;
  let mk = get_image 0 0 500 311 in
  let vect = create_cloud_points_from_image mk in
  save_pixel_array (Array.append vect1 vect) "Milner_Knuth4.txt"
;;

(* hand_made_star_square_cloud_points () saves the modification done on the picture in the file "star_squaretxt"
   No argument
   hand_made_star_square_cloud_points () : unit()
*)
let hand_made_star_square_cloud_points () =
  open_window 1000 500 ;
  resize_window 1000 500;
  let img = get_star () in
  draw_image img 0 0;
  let img = get_square () in
  draw_image img 500 0;
  let mk = get_image 0 0 1000 500 in
  let vect = create_cloud_points_from_image mk in
  save_pixel_array (vect) "star_square.txt"
;;

(* rift_star_square n separates the picture of star and square with n points
   n : int
   rift_star_square n : ((int * int) array, (int * int) array
*)
let rift_star_square n =
  let f = open_in ("star_square.txt") in
  let n = int_of_string (input_line f) in
  let vect_for_star = Array.make (n/2+4) (0,0) in
  let vect_for_square = Array.make (n/2+4) (0,0) in
  vect_for_star.(1) <- (0,499);
  vect_for_square.(1) <- (0,499);
  vect_for_star.(2) <- (499,499);
  vect_for_square.(2) <- (499,499);
  vect_for_star.(3) <- (499,0);
  vect_for_square.(3) <- (499,0);
  for i=0 to n-1 do
    if i mod 2 = 0
    then
      (
	let x = int_of_string (input_line f) in
	let y = int_of_string (input_line f) in
	if x>=500 && x<=999
	then vect_for_square.(i/2+4) <- (x-500,y);
      )
    else
      (
	let x = int_of_string (input_line f) in
	let y = int_of_string (input_line f) in
	if x>=0 && x<=499 then vect_for_star.((i-1)/2+4) <- (x,y);
      )
  done;
  (vect_for_star,vect_for_square)
;;

(* hand_made_milner_mnuth_cloud_points () saves the modification done on the picture in the file "milner_knuth.txt"
   No argument
   hand_made_milner_knuth_cloud_points () : unit()
*)
let hand_made_milner_knuth_cloud_points () =
  open_window 800 600 ;
  resize_window 800 600;
  let img = get_milner ()  in
  draw_image img 0 0;
  let img = get_knuth () in
  draw_image img 400 0;
  let mk = get_image 0 0 800 600 in
  let vect = create_cloud_points_from_image mk in
  save_pixel_array (vect) "milner_knuth1.txt"
;;

(* rift_milner_knuth n separates the picture of milner and knuth with n points
   n : int
   rift_milner_knuth n : ((int * int) array, (int * int) array
*)
let rift_milner_knuth n =
  let f = open_in ("milner_knuth" ^ string_of_int n ^".txt") in
  let n = int_of_string (input_line f) in
  let vect_for_Milner = Array.make (n/2+4) (0,0) in
  let vect_for_Knuth = Array.make (n/2+4) (0,0) in
  vect_for_Milner.(1) <- (0,599);
  vect_for_Knuth.(1) <- (0,599);
  vect_for_Milner.(2) <- (399,599);
  vect_for_Knuth.(2) <- (399,599);
  vect_for_Milner.(3) <- (399,0);
  vect_for_Knuth.(3) <- (399,0);
  for i=0 to n-1 do
    if i mod 2 = 0
    then
      (
	let x = int_of_string (input_line f) in
	let y = int_of_string (input_line f) in
	if x>=400 && x<=799
	then vect_for_Knuth.(i/2+4) <- (x-400,y);
      )
    else
      (
	let x = int_of_string (input_line f) in
	let y = int_of_string (input_line f) in
	if x>=0 && x<=399 then vect_for_Milner.((i-1)/2+4) <- (x,y);
      )
  done;
  (vect_for_Milner,vect_for_Knuth)
;;

(* improve_milner_knuth_cloud_points ()  allows to create new points on the image
   No argument 
   improve_milner_knuth_cloud_points () : unit
*)
let improve_milner_knuth_cloud_points () =
  let vect1 = return_pixel_array_from_txt "milner_knuth3.txt" in
  let (m,k) = rift_milner_knuth 3 in
  let milner = get_milner () in
  let knuth = get_knuth () in
  open_window 800 600 ;
  resize_window 800 600;
  draw_image milner 0 0;
  draw_image knuth 400 0;
  for i=0 to (Array.length m -1) do
    let (x1,y1) = m.(i)
    and (x2,y2) = k.(i)
    in
    set_color green ;
    plot x1 y1 ;
    plot (x2+400) (y2) 
  done;
  let mk = get_image 0 0 800 600 in
  let vect = create_cloud_points_from_image mk in
  save_pixel_array (Array.append vect1 vect) "milner_knuth4.txt"
;;

match choice with
|1 -> let gif = morphing (image1()) (image2()) (creation_points_cloud1 ()) (creation_points_cloud2 ()) nb_step in
     display_gif gif;

|2 -> let gif = morphing (get_strawberry()) (get_raspberry()) (create_points_strawberry ()) (create_points_raspberry ()) nb_step in
      display_gif gif;

|3 -> let (m,k) = rift_Milner_Knuth 4 in
      let gif = morphing (get_Knuth ()) (get_Milner()) k m nb_step in
      display_gif gif;

|4 -> let (m,k)=  rift_milner_knuth 3 in
      let gif = morphing (get_knuth ()) (get_milner()) k m nb_step in
      display_gif gif;

|5 -> let (st,sq)= rift_star_square () in
       let gif = morphing (get_star ()) (get_square ()) st sq nb_step in
       display_gif gif;
|42 -> improve_milner_knuth_cloud_points ();
 
 |_ -> failwith "This is not an option"
 ;;


