open Graphics;;
open Graphic;;
open Matrix;;
open Geometry;;

(*compute x1 x2 x3 y1 y2 y3 computes the coordinate of the center of the circumscribed circle of the three points (x1, y1), (x2, y2) et (x3, y3). y1, y2 and y3 must be different
  x1 : float
  x2 : float
  x3 : float
  y1 : float
  y2 : float
  y3 : float
  compute x1 x2 x3 y1 y2 y3 : float * float*)
let compute x1 x2 x3 y1 y2 y3 =
  let x = -1. *. (((x3**2. -. x2**2. +. y3**2. -. y2**2.) /. (2. *. (y3 -. y2)))
                  -. ((x2**2. -. x1**2. +. y2**2. -. y1**2.) /. (2. *. (y2 -. y1)))
                 )
              /. (((x2 -. x1) /. (y2 -. y1)) -. ((x3 -. x2) /. (y3 -. y2))) in
  let y = -1. *. ((x2 -. x1) /. (y2 -. y1)) *. x +. ((x2**2. -. x1**2. +. y2**2. -. y1**2.) /. (2. *. (y2 -. y1))) in
  x, y
;;  

(*radius x y xc yc computes the radius of the circle centered at (xc, yc) and going through (x, y)
  x : float
  y : float
  xc : float
  yc : float
  radius x y xc yc : float*)  
let radius x y xc yc =
  sqrt((x -. xc)**2. +. (y -. yc)**2.)
;;

(*coordinate_circle point1 point2 point3 computes the the coordinate of the center and the radius of the circumscribed circle of the three points point1 point2 point3
  point1 : point
  point2 : point
  point3 : point
  coordinate_circle point1 point2 point3 : float*float*float *)
let rec coordinate_circle point1 point2 point3 =
  let x1, x2, x3 = get_x point1, get_x point2, get_x point3 in
  let y1, y2, y3 = get_y point1, get_y point2, get_y point3 in
  
  if int_of_float(y1) == int_of_float(y2) 
    then begin
           if x1 == x3 || x2 == x3
            then 
              (let xc = ((x1 +. x2)/.2.) and yc = (y2 +. y3)/.2. in
                (xc, yc, radius x1 y1 xc yc))  
           else coordinate_circle point1 point3 point2
         end  
         
  else begin
        if y2 == y3 
         then begin
                if x1 == x2 || x1 == x3
                 then 
                 (let xc = ((x2 +. x3)/.2.) and yc = (y1 +. y2)/.2. in
                  (xc, yc, radius x1 y1 xc yc)) 
                else coordinate_circle point2 point1 point3
              end
              
        else begin           
               let xc, yc = compute x1 x2 x3 y1 y2 y3 in
               (xc, yc, radius x1 y1 xc yc)
             end
        end
;;

(*check_point_out points triangle checks for each point of the set points if point is in the circumscribed circle of triangle
  points : point_set
  triangle : triangle
  check_point_out points triangle : bool*)
let check_point_out points triangle =
  draw_triangle (get_p1 triangle) (get_p2 triangle) (get_p3 triangle) red;
  let xc, yc, r= coordinate_circle (get_p1 triangle) (get_p2 triangle) (get_p3 triangle) in
  set_color white;
  draw_circle (int_of_float(xc)) (int_of_float(yc)) (int_of_float(r));
  let rec aux points =
    if is_empty_point_set points then true 
    else 
        let point = car_point_set points in
        draw_cross point;
         if get_x point > xc +. r || 
            get_x point < xc -. r || 
            get_y point > yc +. r || 
            get_y point < yc -. r ||
            equal_point (get_p1 triangle) point || 
            equal_point (get_p2 triangle) point || 
            equal_point (get_p3 triangle) point  
          then true && (aux (cdr_point_set points))
        else not(in_circle triangle point) && (aux (cdr_point_set points))
  in 
  
  aux points;
;;
        
(*check_delaunay triangles points checks if the triangulation defined by triangles on the set points is a Delaunay's one
  triangles : triangle_set
  points : point_set
  check_delaunay triangles points : unit*)        
let check_delaunay triangles points =
  print_string("press n to check the next triangle");
  print_newline();
  let rec aux triangles2 =
  if is_empty_triangle_set triangles2 
    then (print_string("Delaunay : check");
         print_newline())
  else begin
        if not(check_point_out points (car_triangle_set triangles2))
         then (print_string("Delaunay : error");
              print_newline())
        else
          (wait_key 'n';
          draw_triangles triangles; 
          aux (cdr_triangle_set triangles2))
      end
  in
  aux triangles
;;


(*test_tilling triangle_set max_x max_y test if triangle_set is a tilling in the windows delimited by max_x and max_y
  triangle_set : triangle_set
  max_x : int
  max_y : int
  test_tilling triangle_set max_x max_y : unit*)
let test_tilling triangle_set max_x max_y =
  let test = ref true in
  test := (((cumulated_surface triangle_set) -. float_of_int ((max_x-1)*(max_y-1)))) < 10.**(-5.);
  if not(!test)
    then (print_string("Tilling : error");
          print_newline())
  else (print_string("Tilling : check");
       print_newline())
;;

(*test_delaunay_tilling triangle_set point_set max_x max_y tests if the triangle set is a tilling of the window defined by max_x max_y and if the tilling is a Delaunay's triangulation
  triangle_set : triangle_set
  point_set : point_set
  max_x : int
  max_y : int
  test_delaunay_tilling triangle_set point_set max_x max_y : unit*)
let test_delaunay_tilling triangle_set point_set max_x max_y =
  (test_tilling triangle_set max_x max_y);
  (check_delaunay triangle_set point_set)  
;;
