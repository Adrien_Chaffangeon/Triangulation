open Geometry;;

type matrix = float array array;;       
  
(*make_matrix m n create a matrix with m lines and n columns filled with (flotting) zero
  m : int
  n : int
  make_matrix m n : matrix*)
let make_matrix m n =
  let matrix = Array.make m (Array.make n 0.) in
  for i = 1 to m-1 do
    matrix.(i) <- Array.make n 0.
  done;
  matrix
;;

(*set_cell m i j p put p in the cell line i column j in matrix m
  m : matrix 
  i : int
  j : int
  p : float
  set_cell m i j p : unit*)
let set_cell m i j p =
  if (i < 0 || j < 0 || i > (Array.length m -1) || j > (Array.length m.(0) -1))
  then failwith "incomptable arguments"
  else (m.(i)).(j)<-p
;;

(*get_cell m i j returns the value in the cell line i column j
  m : matrix 
  i : int
  j : int
  get_cell m i j : float*)
let get_cell m i j =
  if (i < 0 || j < 0 || i > (Array.length m -1) || j > (Array.length m.(0) -1))
  then failwith "incomptable arguments"
  else (m.(i)).(j)
;;

(*print_matrix m print the matrix m in the terminal
  m : matrix
  print_matrix m : unit*)
let print_matrix m =
  for i=0 to (Array.length m)-1 do
    for j=0 to (Array.length m.(0))-1 do
      print_float(m.(i).(j))
    done;
    print_newline()
  done
;;  
  
(*exchange_lign matrix i j exchange the line i with the line j of the matrix matrix
  matrix : matrix
  i : int
  j : int
  exchange_lign matrix i j : unit*)    
let exchange_lign matrix i j =
  let l = Array.length matrix in
  if (i<0 || i>(l-1) || j<0 || j>(l-1))
    then failwith"Invalid index"
  else  (let temp = matrix.(i) in
         matrix.(i) <- matrix.(j);
         matrix.(j) <- temp)
;;

(*substract_column matrix j i coef change the column j by (column j - coef*column i)
  matrix : matrix
  j : int
  i : int
  coef : float
  substract_column matrix j i coef : unit*)
let substract_column matrix j i coef =
  let l = Array.length matrix.(0) in
  if (i<0 || i>(l-1) || j<0 || j>(l-1))
    then failwith"Invalid index"
  else  (for k=0 to (Array.length matrix)-1 do
       matrix.(k).(j) <- matrix.(k).(j) -. coef *. matrix.(k).(i)
       done;)
;;         

(*in_circle t p returns true if the point p is in circumscribed circle ot the triangle t
  t : triangle
  p : point
  in_circle t p : bool*)
let in_circle t p = 
  if   (equal_point (get_p1 t) p || 
       equal_point (get_p2 t) p || 
       equal_point (get_p3 t) p || 
       equal_point (get_p1 t) (get_p2 t) ||  
       equal_point (get_p1 t) (get_p3 t) || 
       equal_point (get_p3 t) (get_p2 t) )
    then failwith"Invalid point"
  else (let point_into_matrix a b c d = (* we work on the transposed matrix, with an exchange between the first and the last line *)
          let m = make_matrix 4 4 in
          set_cell m 1 0 (get_y a);
          set_cell m 1 1 (get_y b);
          set_cell m 1 2 (get_y c);
          set_cell m 1 3 (get_y d);
          set_cell m 2 0 ((get_x a)**2. +. (get_y a)**2.);
          set_cell m 2 1 ((get_x b)**2. +. (get_y b)**2.);
          set_cell m 2 2 ((get_x c)**2. +. (get_y c)**2.);
          set_cell m 2 3 ((get_x d)**2. +. (get_y d)**2.);
          set_cell m 3 0 (get_x a);
          set_cell m 3 1 (get_x b);
          set_cell m 3 2 (get_x c);
          set_cell m 3 3 (get_x d);
          for i=0 to 3 do
            set_cell m 0 i 1.
          done;
          m
        in
        
        let determinant matrix = (* the four points are supposed different *)
          let factor = ref (-1.) in
          substract_column matrix 1 0 1.;
          substract_column matrix 2 0 1.;
          substract_column matrix 3 0 1.;
          if   (get_cell matrix 1 1)<>0.
          then   (substract_column matrix 2 1 ((get_cell matrix 1 2)/.(get_cell matrix 1 1));
               substract_column matrix 3 1 ((get_cell matrix 1 3)/.(get_cell matrix 1 1)) )
          else   (substract_column matrix 2 1 ((get_cell matrix 3 2)/.(get_cell matrix 3 1));
               substract_column matrix 3 1 ((get_cell matrix 3 3)/.(get_cell matrix 3 1));
               exchange_lign matrix 1 3;
               factor := !factor *. (-1.) );
          let little_det = (get_cell matrix 2 2) *. (get_cell matrix 3 3) -. ((get_cell matrix 2 3) *. (get_cell matrix 3 2)) in
          let det = !factor *. ((get_cell matrix 0 0) *. (get_cell matrix 1 1) *. little_det ) in
          det  
        in
        
        let matrix = point_into_matrix (get_p1 t) (get_p2 t) (get_p3 t) p in
        let d = determinant matrix in
        d>0.);
;;
