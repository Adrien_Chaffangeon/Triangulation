Delaunay's triangulation.
*******************************************
This is the README for Delaunay's triangulation on a points cloud in two dimensions.
*******************************************
There is an application of this triangulation with morphing.
*******************************************
Be carefull, you must have the librairy graphics.cma to use this project.
Also, the test mode create a file, result.data, grade to be use with R, the separator is ",".
*******************************************
To compile them use the makefile program with the command make (you can also use
make clean to clean temporary files and object files).
To run it with x points, use make run Nb_point=x.
*******************************************
To test it : use make then use make test Nb_test=x , with x a number.
Test computes the number of calculating determinant for a cloud of points with y points, for y 
between 1 and Nb_test.
*******************************************
You can find the wiki on : https://gitlab.com/Adrien_Chaffangeon/Triangulation .
*******************************************
If you don't have the archive, you can have the code on : 
git@gitlab.com:Adrien_Chaffangeon/Triangulation.git .
*******************************************
Authors :
Adrien Chaffangeon
Benjamin Bordais
Timothee Anne
*******************************************
To contact us :
Adrien.Chaffangeon@ens-rennes.fr
Benjamin.Bordais@ens-rennes.fr
Timothee.Anne@ens-rennes.fr
*******************************************
This is an open-source project, enjoy yourself to enhance its!
