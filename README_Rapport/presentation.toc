\beamer@endinputifotherversion {3.36pt}
\select@language {french}
\beamer@sectionintoc {2}{Triangulation de Delaunay}{4}{0}{1}
\beamer@subsectionintoc {2}{1}{R\IeC {\'e}solution du probl\IeC {\`e}me}{5}{0}{1}
\beamer@subsectionintoc {2}{2}{Validation}{15}{0}{1}
\beamer@sectionintoc {3}{Morphing}{17}{0}{2}
\beamer@subsectionintoc {3}{1}{Introduction Morphing}{18}{0}{2}
\beamer@subsubsectionintoc {3}{1}{1}{Lecture d'une image}{19}{0}{2}
\beamer@subsubsectionintoc {3}{1}{2}{Cr\IeC {\'e}ation d'un nuage de points}{20}{0}{2}
\beamer@subsubsectionintoc {3}{1}{3}{Calcul des transformations}{23}{0}{2}
\beamer@subsubsectionintoc {3}{1}{4}{R\IeC {\'e}sultat}{27}{0}{2}
\beamer@sectionintoc {4}{Conclusion}{35}{0}{3}
