\documentclass[a4paper,11pt]{article}

\usepackage{fullpage}%
\usepackage[T1]{fontenc}%
\usepackage[utf8]{inputenc}%
\usepackage[main=francais,english]{babel}%

\usepackage{graphicx}%
\usepackage{url}%
\usepackage{abstract}%

\usepackage{mathpazo}%

\usepackage{listings}%

\lstMakeShortInline{|}%

\lstset{%
  basicstyle=\sffamily,%
  columns=fullflexible,%
  language=Caml,%       % À ajuster dans chaque projet!
  frame=lb,%
  frameround=fftf,%
}%

\parskip=0.5\baselineskip

\sloppy

\begin{document}

\title{Triangulation de Delaunay}

\author{Timothée Anne, Benjamin Bordais, Adrien Chaffangeon}

\date{}

\maketitle

\begin{abstract}
  Ce projet a pour but d'implémenter la triangulation de Delaunay et de 
  l'appliquer à un nuage de points quelconques. Par la suite, 
  on étudiera comment on peut utiiser cette méthode pour implémenter la 
  technique de \emph{morphing}.
  \begin{description}
  \item[Mots-clés:] Delaunay; Triangulation; Nuage de points.
  \end{description}
\end{abstract}

\section*{Introduction}

On considère un nuage de points. Le problème consiste à paver l'espace 
à l'aide de triangles dont les sommets correspondraient aux points 
donnés. On cherche à rendre ces triangles les plus réguliers possibles 
(i.e \emph{les plus équilatéraux possibles}). Pour cela, on procède 
de manière récursive en considérant un maillage initial et en rajoutant 
au fur et à mesure chaque point en appliquant l'algorithme de Delaunay. 
De prime abord, nous verrons comment nous avons implémenter la 
triangulation de Delaunay et les algorithmes mis en \oe uvre 
(Section ~\ref{sec:triangulation}), puis nous verrons l'application de 
cette triangulation au phénomène de morphing 
(Section ~\ref{sec:extension}).

\section{Triangulation de Delaunay}
\label{sec:triangulation}

\subsection{Structures de données et modules de base}
\subsubsection{Module \lstinline{Geometry}}
Nous travaillons à l'aide des quatre types suivant : 
\begin{itemize}
\item |type point = {x:float; y:float}|;
\item |type triangle = {p1:point; p2:point; p3:point}|;
\item |type point_set = point list|;
\item |type triangle_set = triangle list|.
\end{itemize}
Nous avons implémenté les fonctions usuelles qui permettent de 
travailler avec ces types, qui seront abstraits par la suite. 
Nous avons incorporé à ce module la fonction |border| qui détermine 
les arêtes qui délimitent une zone convexe de l'espace définie par 
une liste de triangles. Cette fonction a été placé ici car 
elle ne nécessite aucun autre module pour fonctionner. Cette fonction 
est fondamentale dans la mise en place de la triangulation de Delaunay. 
La fonction qui permet de savoir si un triangle est direct se situe 
dans ce module et est nommée |is_direct_direction|.

\subsubsection{Module \lstinline{Matrix}}
On introduit ici le type |Matrix| définie ainsi : 
|type matrix = float array array|. Ce module a été créé pour mettre en 
place la fonction |in_circle| qui détermine si un point appartient 
au cercle criconscrit à un triangle. Cela nécessite le calcul 
d'un déterminant, et donc par suite, quelques manipulations de matrices. 
Plusieurs fonctions usuelles sur les matrices sont définies : 
|make_matrix| pour la création de matrice, |set_cell| pour changer 
la valeur d'une case, |get_cell| pour récupérer la valeur d'une case, 
|exchange_lign| qui échange deux lignes ou encore 
|substract_column| qui effectue une opération de transvection. 
Ces fonctions nous ont permis de nous ramener au calcul aisé 
d'un déterminant d'une matrice triangulaire par blocs, à l'aide d'un 
algorithme analogue à celui du pivot de Gauss.

\subsubsection{Module \lstinline{Graphic}}
Ce module regroupe toutes les fonctions destinées à régler 
l'affichage graphique. Elles ne sont donc pas utilisées dans 
l'implémentation de la fonction Delaunay, mais uniquement dans 
l'affichage du résultat une fois les calculs effectués.

\subsection{Algorithme principal}
On souhaite trianguler, à l'aide de l'algorithme de Delaunay, 
l'ensemble des points de la liste |points|. Pour implémenter 
cet algorithme, nous avons d'abord créé le pavage formé par 
deux triangles dont les sommets correspondent aux points 
extremaux de la fenêtre d'affichage. Ces triangles sont incorporés 
à une liste de triangles |triangles|, initialement vide, 
qui réprésente l'ensemble des triangles formant la triangulation 
en cours. A ce moment, aucun des éléments de |points| n'est triangulé. 
On incorpore alors les points les uns après les autres à 
la triangulation. Lors de l'ajoût d'un point p au pavage, 
on procède de la manière suivante.
\begin{itemize}
\item On repère chacun des triangles de |triangles| tel que p 
appartienne à leur cercle circonscrit. Cette opération est effectuée 
par la première partie de la fonction |add_point|. Ces triangles sont 
alors retirés de |triangles|.
\item La réunion de ces triangles constituant une partie convexe 
de l'espace, on peut alors trouver les arêtes qui définissent la 
frontière de cette zone. Cela est pris en charge par la fonction 
|border|.
\item Les triangles formés des extrémités des arêtes et du point p 
sont alors rajoutés à |triangles|. La deuxième partie de la fonction 
|add_point| s'occupe de cette opération. 
\end{itemize}

\subsection{Validation}

\subsubsection{Module \lstinline{Check}}
\paragraph\
Nous avons souhaité vérifier que la triangulation issue de 
notre algorithme conduisait bien à un pavage de Delaunay. 
Plus précisément, étant donné une liste de points, 
une liste de triangles et deux entiers délimitant 
la fenêtre d'affichage, on veut établir que ces triangles pavent 
l'espace, tout en vérifiant la propriété de Delaunay : 
pour tout triangle,aucun des points n'appartient au cercle circonscrit 
à ce triangle. Pour cela, nous avons mis en place le module |Check|.
%
\paragraph\
Pour effectuer une vérification adéquate, on part du principe que 
la triangulation a été crée en partant des points extrémaux de 
la fenêtre considérée (comme cela est fait dans notre algorithme). 
Ce choix a été motivé par le fait que la vérification effectuée n'est 
réalisée que pour vérifier la correction de notre algorithme, 
cela entraine que celle-ci n'est pas transposable sur 
une liste quelconque de triangles dont on ne connaîtrait pas l'origine.
%
\paragraph\
Dans un premier temps, on souhaite vérifier la propriété de Delaunay. 
On parcout la liste de points que nous avons. Pour chacun d'entre eux, 
on commence par vérifier qu'il n'appartient pas à un carré entourant 
le cercle circonscrit au triangle. Cela est réalisé pour limiter 
le nombre de calcul de déterminant (très couteux) dans ce test. 
Dans le cas où le point considéré appartiendrais à ce carré, 
on fait alors appel à la fonction |in_circle| (du module |Geometry|). 
Cette opération de vérification est opérée par la fonction 
|check_delaunay|. 
%
\paragraph\
Dans un second temps, on désire établir que la liste de triangles est 
un pavage de l'espace. C'est à dire que chaque point de l'espace à paver 
appartient à un et un seul triangle (sauf les points de 
la frontière entre triangles). Comme la triangulation est initialisée 
sur les points extrémaux de l'espace à paver, on est assuré que tous 
les points appartiennent à la réunion des triangles. On vérifie alors 
que l'air cumulé de tous les triangles considérés est égal à l'air 
total de la fenêtre graphique. La fonction |test_tilling| prend en 
charge cette opération. 
%
\paragraph\
Ainsi, on a une triangualtion de Delaunay si et seulement si 
les deux tests précédents sont vérifiés. On souhaite à présent 
étudier la complexité de notre algorithme.
%
\subsubsection{Etude de la complexité}
On souhaite mesurer la rapidité de notre algorithme en faisant en sorte 
que cette mesure ne dépende pas de la machine sur laquelle 
on l'effectue. Ainsi, on ne doit pas mesurer le temps nécessaire 
pour executer l'algorithme. Nous avons donc choisis de compter 
le nombre de fois que l'on fait appel à la fonction |in_circle|. 
En effet, chaque appel à cette fonction entraîne le calcul 
d'un déterminant d'une matrice de dimension $4 \times 4$, 
ce qui est assez couteux. Nous avons donc tracé la courbe qui montre 
l'évolution de la racine carré du nombre de calcul de déterminant 
en fonction du nombre de points à trianguler. On constate alors, 
que la courbe obtenu est linéaire de coefficient directeur égal à 1. 
On en déduit que la complexité de notre algorithme, en fonction du 
nombre de calcul de déterminant, en $O(n^{2})$.
%
\begin{figure}
\begin{center}
\includegraphics[width=0.5\textwidth]{courbe.png}
\caption{Evolution de la racine carré du nombre de calcul de déterminant 
en fonction du nombre de points à trianguler}
\end{center}
\end{figure}
%
\section{Extension}
\label{sec:extension}
%
\subsection{Contribution}
\subsubsection{Le morphing}
Nous avons choisis d'implémenter la technique de morphing, c'est à dire 
le passage d'une image à une autre par des transformations continues. 
Pour réaliser cette extension, nous avons eu besoin de créer 
un nombre important de fonctions intermédiaires, attendu que 
le calcul de la triangulation de Delaunay n'est qu'une des parties 
de l'algorithme de morphing.

\subsubsection{Lecture d'une image}
Dans un premier temps, il faut pouvoir charger des images 
aux formats .jpg, .png, .bmp, ... Pour ce faire nous avons d'abord 
utilisé le module |Camlimages| qui permet de charger de telles images 
au format du module |Graphics|. Puis, au vu de la difficulté 
de compilation sur tous les ordinateurs, nous avons décider 
de créer deux fonctions :
\begin{itemize}
\item |save_image| qui permet d'ouvrir une image quelconque et de 
la sauvegarder dans un fichier .txt en conservant, pour 
pour chaque pixel, les trois compasantes RGB;
\item |load_image_from_txt| qui permet de lire un fichier .txt et de 
le transformer en image du module |Graphics|.
\end{itemize}
Ainsi, après avoir sauvergarder de cette façon toutes les images 
que l'on utilise, nous avons pu nous passer du module 
|Camlimages| et alléger la compilation.

\subsubsection{Création d'un nuage de points}
Nous avons décider de créer les nuages de points à la main. 
Pour ce faire, nous utilisons la fonction 
|create_cloud_points_from_image| pour ouvrir les images côte à côte 
et sélectionner à la souris, sur la fenêtre |Graphics|, 
les pixels à entrer. Pour bien créer des pixels de même rôle 
pour chaque image, nous créons succesivement chaque couple de points. 
Ces nuages dépassant le millier de points, nous les sauvegardont aussi 
dans des fichiers .txt, de manière similaire à ce que nous utilisons 
pour les images, à l'aide de |save_pixel_array| et 
|return_pixel_array_from_txt|. Nous avons aussi ajouter les fonctions 
|rift_image1_image2| qui permet de calculer les deux nuages de points 
à partir de ceux créés et |improve_image1_image2_cloud_points| pour 
pouvoir ajouter des points à un nuage de points déjà créé.

\subsubsection{Calcul des transformations}
L'algorithme en lui-même se décompose en plusieurs étapes. On suppose 
dans la suite que l'on veut créer $n$ étapes à partir des images 
\emph{A} et \emph{B} et de leur nuage de points \emph{Ca} et \emph{Cb}.
\begin{itemize}
\item On crée, à l'aide de |create_matrix_barycentre|, la matrice 
qui contient les coordonnées de chaque point, dnas chacune 
des $n$ images, à l'aide d'une barycentration des coordonnées 
dans \emph{Ca} et \emph{Cb}, de façon à obtenir une 
déformation continue.
\item On calcule la triangulation de Delaunay du 
nuage de points central, afin de garder le plus possible, lors 
des déformations, ses propriétés, à l'aide de la fonction Delaunay 
définie plus haut. 
\item On utilise à présent la fonction |morphing| pour procéder 
au calcul des images intermédiaires.
\item Pour chaque triangle et à chaque étape, on détermine sa couleur 
à l'aide de |average_color|. 
\item Pour ce faire, on calcule les pixels présents dans ce triangle 
grâce à |pixels_in_triangle|, puis on calcule, pour chacun d'eux, 
le barycentre pondéré (selon l'étape que l'on considère) 
de ses composantes RGB entre celles de \emph{A} et de l'image \emph{B}. 
On effectue alors la moyenne sur tous ces pixels.
\end{itemize}

\subsection{Validation}
Notre travail a donc abouti au morphing entre plusieurs images. On peut 
constater que le résultat obtenu est conforme à ce que l'on attendait, 
comme on peut le voir sur la figure 2. Nous avons réalisé d'autres 
morphings, comme par exemple entre une étoile et un carré de couleurs 
différentes. Le résultat est alors plus probant car les images sont 
beaucoup plus simples, et nécessitent beaucoup moins de points 
pour obtenir un résultat satisfaisant.
\begin{figure}
\includegraphics[width=1\textwidth]{transformation.png}
\caption{Morphing de deux grands Informaticiens}
\end{figure}
%%
\section{Conclusion}
\paragraph\
Nous avons donc pu implémenter l'algorithme de base de Delaunay, puis 
la vérification de celui-ci. De plus, la méthode de morphing a pu 
être mise en place. Les résultats obtenus sont assez concluants, 
de plus tous les algorithmes peuvent être exécutés sur 
chacune de nos machines, ce qui montre qu'ils ne sont 
pas trop coûteux en terme de complexité.
%
\paragraph\
Cependant, notre travail est incomplet. En effet, la fonction 
de vérification n'est que partielle car elle suppose que la liste 
de triangles a été crée d'une certaine manière. 
On pourrait exiger que celle-ci fonctionne sur toutes les distributions. 
Par exemple, un appel à la fonction |border| permettrait de déterminer 
la frontière de la zone en question et ainsi de vérifier que 
tous les points de la fenêtre appartiennent à un triangle. 
De même, une mise en place d'un nombre plus important de points 
permettrait également d'obtenir un résultat plus concluant.
 
\end{document}



%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
